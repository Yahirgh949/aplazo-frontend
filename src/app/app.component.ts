import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SaveLoanService} from "./services/save-loan.service";
import {PaymentPlanModel} from "./models/payment-plan.model";
import {take} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'aplazo-frontend';

  paymentPlan: PaymentPlanModel[] = [];


  constructor(private service: SaveLoanService) {
  }

  formLoan = new FormGroup({
    amount: new FormControl('', [Validators.required, Validators.min(1), Validators.max(999999)]),
    terms: new FormControl('', [Validators.required, Validators.min(4), Validators.max(52)]),
    rate: new FormControl('', [Validators.required, Validators.min(1), Validators.max(100)]),
  });

  save() {
    this.service.save(this.formLoan.value).pipe(take(1)).subscribe((value) => {
      this.paymentPlan = value;
      this.formLoan.reset();
    });
  }
}
