export class PaymentPlanModel {
  paymentNumber: number;
  paymentAmount: number;
  pendingAmount: number;
  paymentDate: Date;


  constructor(paymentNumber: number, paymentAmount: number, pendingAmount: number, paymentDate: Date) {
    this.paymentNumber = paymentNumber;
    this.paymentAmount = paymentAmount;
    this.pendingAmount = pendingAmount;
    this.paymentDate = paymentDate;
  }
}
