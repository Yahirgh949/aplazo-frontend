import { TestBed } from '@angular/core/testing';

import { SaveLoanService } from './save-loan.service';

describe('SaveLoanService', () => {
  let service: SaveLoanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaveLoanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
