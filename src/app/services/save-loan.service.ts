import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {PaymentPlanModel} from "../models/payment-plan.model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SaveLoanService {

  private url = 'http://localhost:8080/loan';

  constructor(private http: HttpClient) {
  }



  save(loan: any): Observable<PaymentPlanModel[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<any>(this.url, loan, httpOptions);
  }
}
